@extends('tpl.main')

@section('title', 'Single Comment')

@section('content')
    <table class="table table-bordered">
        <tr>
            <td>NickName:</td>
            <td>{{ $comment->nickname }}</td>
        </tr>
        <tr>
            <td>Email:</td>
            <td>{{ $comment->email }}</td>
        </tr>
        <tr>
            <td>Website:</td>
            <td>{{ $comment->website }}</td>
        </tr>
        <tr>
            <td>Comment:</td>
            <td>{{ $comment->comment }}</td>
        </tr>
        <tr>
            <td>Created at:</td>
            <td>{{ $comment->created_at }}</td>
        </tr>
        <tr>
            <td>Last updated at:</td>
            <td>{{ $comment->updated_at }}</td>
        </tr>
    </table>
    <table class="table table-bordered text-center mx-auto w-75">
        <thead class="thead-dark">
            <tr>
                <th colspan="2">Associate Post</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Post ID</td>
                <td>Post Title</td>
            </tr>
            <tr>
                <td>{{ $comment->Post->id }}</td>
                <td><a href="{{ route('post.show', [ 'post' => $comment->Post->id ]) }}" target="_blank">{{ $comment->Post->title }}</a></td>
            </tr>
        </tbody>
    </table>
@endsection