@extends('tpl.main')

@section('title', 'The data you obtained through api')

@section('content')
    <div class="alert alert-info w-75 mx-auto text-center" role="alert">
        <p>Why am I redirected to this page？= =</p>
        <p>You didn't send the data of "return_url" field when you used our api.</p>
    </div>
    <div class="alert alert-info w-75 mx-auto" role="alert">
        <p class="text-center">Our system will return data which are composed by the following list. &gt; O &lt;</p>
        <ul>
            <li>status(integer)</li>
            <li>message(string)</li>
            <li>Response(string|json)</li>
        </ul>
    </div>
    <table class="table table-dark mb-2">
        <tr>
            <td>Status</td>
            <td>{{ $status }}</td>
        </tr>
        <tr>
            <td>Message</td>
            <td>{{ $message }}</td>
        </tr>
        <tr>
            <td>Response</td>
            <td>{{ $Response }}</td>
        </tr>
    </table>
@endsection