@extends('tpl.main')

@section('title', $post->title .' edit')

@section('content')
    <form action="{{ route('post.update', [ 'post' => $post->id ]) }}" method="post">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="user_id">User id:</label>
            <select class="form-control" name="user_id" id="user_id">
                @foreach($users as $user)
                    @if($user->id == $post->user_id)
                        <option value="{{ $user->id }}" selected>{{ $user->name }}</option>
                    @else
                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="title">Title:</label>
            <input type="text" name="title" id="title" class="form-control" value="{{ $post->title }}">
        </div>
        <div class="form-group">
            <label for="content">Content:</label>
            <textarea name="content" id="content" class="form-control" rows="3">{{ $post->content }}</textarea>
        </div>
        <div class="form-group">
            <label for="catalog">Catalog:</label>
            <select name="catalog[]" id="catalog" class="form-control" multiple>
                @foreach($catalogs as $catalog)
                    @if(in_array($catalog->id, $catalogList))
                        <option value="{{ $catalog->id }}" selected>{{ $catalog->catalog_name }}</option>
                    @else
                        <option value="{{ $catalog->id }}">{{ $catalog->catalog_name }}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="created_at">Created at:</label>
            <input type="text" name="created_at" id="created_at" class="form-control" value="{{ $post->created_at }}" readonly="readonly">
        </div>
        <div class="form-group">
            <label for="updated_at">Updated at:</label>
            <input type="text" name="updated_at" id="updated_at" class="form-control" value="{{ $post->updated_at }}" readonly="readonly'">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Update!</button>
        </div>
    </form>
@endsection