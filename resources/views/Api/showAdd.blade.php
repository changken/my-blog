@extends('tpl.main')

@section('title', 'Get New Api Key')

@section('content')
        <form action="{{ route('api.add') }}" method="post">
            @csrf
            <div class="form-group">
                <label for="key">KEY:</label>
                <input type="text" name="key" id="key" class="form-control" placeholder="Key" value="{{ $key }}">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
@endsection