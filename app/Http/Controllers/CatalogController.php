<?php

namespace App\Http\Controllers;

use App\Repositories\CatalogRepository;
use Illuminate\Http\Request;

class CatalogController extends Controller
{
    protected $catalogRepository;

    public function __construct(CatalogRepository $catalogRepository)
    {
        $this->catalogRepository = $catalogRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('catalog.index',[
            'catalogs' => $this->catalogRepository->paginate(5)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('catalog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->catalogRepository->create([
            'catalog_name' => $request->input('catalog_name')
        ]);

        return view('tpl.msg', [
            'title' => '新增成功',
            'status' => 1,
            'msg' => '新增成功！'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('catalog.show',[
            'catalog' => $this->catalogRepository->getById($id, 'Post')
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('catalog.edit', [
            'catalog' => $this->catalogRepository->getById($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->catalogRepository->update($id,[
            'catalog_name' => $request->input('catalog_name')
        ]);

        return view('tpl.msg', [
            'title' => '修改成功',
            'status' => 1,
            'msg' => '修改成功！'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->catalogRepository->delete($id);

        return view('tpl.msg', [
            'title' => '刪除成功',
            'status' => 1,
            'msg' => '刪除成功！'
        ]);
    }
}
