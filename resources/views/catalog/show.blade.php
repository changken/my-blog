@extends('tpl.main')

@section('title', $catalog->catalog_name)

@section('content')
    <table class="table table-bordered">
        <tr>
            <td>Catalog ID:</td>
            <td>{{ $catalog->id }}</td>
        </tr>
        <tr>
            <td>Catalog Name:</td>
            <td>{{ $catalog->catalog_name }}</td>
        </tr>
        <tr>
            <td>Amount:</td>
            <td>{{ $catalog->Post()->count() }}</td>
        </tr>
        <tr>
            <td>Created at:</td>
            <td>{{ $catalog->created_at }}</td>
        </tr>
        <tr>
            <td>Last updated at:</td>
            <td>{{ $catalog->updated_at }}</td>
        </tr>
    </table>
    <table class="table table-bordered text-center mx-auto w-75">
        <thead class="thead-dark">
            <tr>
                <th colspan="2">Associate Post List</th>
            </tr>
        </thead>
        <tbody>
        <tr>
            <td>Post ID</td>
            <td>Post Title</td>
        </tr>
            @foreach($catalog->Post as $post)
                <tr>
                    <td>{{ $post->id }}</td>
                    <td><a href="{{ route('post.show', [ 'post' => $post->id ]) }}" target="_blank">{{ $post->title }}</a></td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection