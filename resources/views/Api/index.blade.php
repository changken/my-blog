@extends('tpl.main')

@section('title', 'Your Api Key')

@section('content')
    <div class="alert alert-warning w-50 mx-auto" role="alert">
        <p class="text-center">Please keep your api key secretly!</p>
    </div>
    @foreach($user->Api as $api)
        <table class="table table-bordered">
            <tr>
                <td>KEY ID:</td>
                <td>{{ $api->id }}</td>
            </tr>
            <tr>
                <td>KEY:</td>
                <td>{{ $api->key }}</td>
            </tr>
            <tr>
                <td>Created at:</td>
                <td>{{ $api->created_at }}</td>
            </tr>
            <tr>
                <td>Expire Date:</td>
                <td>{{ $api->expire_date }}</td>
            </tr>
        </table>
    @endforeach
@endsection