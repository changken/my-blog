@extends('tpl.main')

@section('title', 'All Posts')

@section('head')
    <script>
    function deleteCheck()
    {
        return confirm('確認要刪除嗎？');
    }
    </script>
@endsection

@section('content')
    @foreach($posts as $post)
        <table class="table table-bordered">
            <tr>
                <td>Title:</td>
                <td><a href="{{ route('post.show', [ 'post' => $post->id ]) }}">{{ $post->title }}</a></td>
            </tr>
            <tr>
                <td>Content:</td>
                <td>{{ $post->content }}</td>
            </tr>
            <tr>
                <td>Catalog:</td>
                <td>
                @foreach($post->Catalog as $cata)
                        <span class="badge badge-secondary" style="font-size:15px;">{{ $cata->catalog_name }}</span>
                @endforeach
                </td>
            </tr>
            <tr>
                <td>Posts Author:</td>
                <td>{{ $post->User->name }}</td>
            </tr>
            <tr>
                <td>Created at:</td>
                <td>{{ $post->created_at }}</td>
            </tr>
            <tr>
                <td>Last updated at:</td>
                <td>{{ $post->updated_at }}</td>
            </tr>
            <tr>
                <td>Action:</td>
                <td>
                    <form action="{{ route('post.destroy', [ 'post' => $post->id ]) }}" method="post">
                        @csrf
                        @method('delete')
                        <button type="button" onclick="document.location.href='{{ route('post.edit', ['post' => $post->id]) }}';" class="btn btn-warning"><i class="fa fa-edit"></i> 編輯</button>
                        <button type="submit" class="btn btn-danger" onclick="return deleteCheck()"><i class="fa fa-trash"></i> 刪除</button>
                    </form>
                </td>
            </tr>
        </table>
    @endforeach
    {{ $posts->links() }}
@endsection