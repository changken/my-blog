@extends('tpl.main')

@section('title', '403 forbidden')

@section('content')
    <div class="alert alert-danger" role="alert">
        <h2><i class="fas fa-exclamation-triangle"></i> {{ $exception->getMessage() }}</h2>
        <h3><i class="fas fa-exclamation-triangle"></i> 講中文即是請您「滾」~XD</h3>
    </div>
@endsection