<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
        * @var  array $guarded
        */
    protected $guarded = [
        'id',
        'created_at',
        'updated_at'
    ];

    public function User()
    {
        return $this->belongsTo(User::class);
    }
	
	public function Comment()
	{
		return $this->hasMany(Comment::class);
	}

    public function Catalog()
    {
        return $this->belongsToMany(Catalog::class);
    }
}
