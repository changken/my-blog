@extends('tpl.main')

@section('title', 'New Catalog')

@section('content')
    <form action="{{ route('catalog.store') }}" method="post">
        @csrf
        <div class="form-group">
            <label for="catalog_name">Catalog Name:</label>
            <input type="text" name="catalog_name" id="catalog_name" class="form-control" placeholder="Catalog Name">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
@endsection