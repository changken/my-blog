<?php

function RandKey($length)
{
    $word = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    $text = "";

    for($i=1;$i<=$length;$i++)
    {
        $item = mt_rand(0, 35);
        $text .= $word[$item];
    }

    return $text;
}

//Api::findOrFail() return collection || model
//that is ridiculous!
/**
 *  api token  check function
 * @param array$api
 * @param array $from
 * @return bool/
 */
function apiTokenCheck(array $api, array $from)
{
    $check_arr = [
        "id" => $api['id'],
        "key" => $api['key'],
        "time" => $from['time'],
        "return_url" => $from['return_url']
    ];
    ksort($check_arr);
    $tokenCheck = strtoupper(hash("sha256", md5(http_build_query($check_arr))));

    return $from['token'] == $tokenCheck;
}
