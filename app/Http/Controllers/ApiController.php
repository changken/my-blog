<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use App\Repositories\ApiRepository;
use Auth;
use Carbon\Carbon;

class ApiController extends Controller
{
    protected $apiRepository;
    protected $userRepository;

    public function __construct(ApiRepository $apiRepository, UserRepository $userRepository)
    {
        $this->apiRepository = $apiRepository;
        $this->userRepository = $userRepository;
    }

    public function index()
    {
        return view('Api.index',[
            'user' => $this->userRepository->getById(Auth::id(), 'Api')
        ]);
    }

    public function showAdd()
    {
        return view('api.showAdd',[
            "key" => RandKey(30)
        ]);
    }

    public function add(Request $request)
    {
        $this->apiRepository->create([
            'user_id' => Auth::id(),
            'key' => $request->input('key'),
            'expire_date' => Carbon::now()->addMonth(1)
        ]);

        return view("tpl.msg",[
            "title" => "The api key saved successfully!",
            "status" => 1,
            "msg" => "The api key saved successfully!"
        ]);
    }

    public function getPosts(Request $request)
    {
        $id = $request->input('id');
        $token = $request->input('token');
        $time = $request->input('time');
        $return_url = $request->input('return_url');

        if (!($request->filled("id") && $request->filled("token") && $request->filled("time") && $request->filled('return_url')))
        {
            return view("Api.showResponse", [
                "status" => 0,
                "message" => "The data is not complete!",
                "Response" => "The data is not complete!"
            ]);
        }
        else {
            //get api collection || model
            $api = $this->apiRepository->getById($id, 'User.Post');

            if($this->apiRepository->isExpired($id))
            {
                return view("Api.response", [
                    "status" => 1,
                    "message" => "The api key is expired!",
                    "Response" => "The api key is expired!",
                    "return_url" => $return_url
                ]);
            }
            elseif (!apiTokenCheck([
                'id' => $api->id,
                'key' => $api->key
            ], [
                'token' => $token,
                'time' => $time,
                'return_url' => $return_url
            ]))
            {
                return view("Api.response", [
                    "status" => 2,
                    "message" => "The Api token is invalid or Api key is not found!",
                    "Response" => "The Api token is invalid or Api key is not found!",
                    "return_url" => $return_url
                ]);
            }
            else
            {
                //via relationship through User model
                $posts = $api->User->Post->toJson();//use json response

                return view("Api.response", [
                    "status" => 100,
                    "message" => "Successful!",
                    "Response" => $posts,
                    "return_url" => $return_url
                ]);
            }
        }
    }
}
