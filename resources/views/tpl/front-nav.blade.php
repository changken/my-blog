<ul class="nav nav-tabs">
    <li class="nav-item">
        <a href="{{ route('blog.home') }}" class="nav-link">Blog</a>
    </li>
    @if(Auth::check())
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
               aria-expanded="false">
                admin <span class="caret"></span>
            </a>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="{{ route('post.index') }}">Post List</a>
                <a class="dropdown-item" href="{{ route('catalog.index') }}">Catalog List</a>
                <a class="dropdown-item" href="{{ route('comment.index') }}">Comment List</a>
            </div>
        </li>
    @endif
</ul>