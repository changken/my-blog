<?php

namespace App\Repositories;

abstract class Repository
{
    protected $model;

    /**
        *@param \Illuminate\Database\Eloquent\Model $model
        *@return void
        */
    public function __construct($model)
    {
        $this->model = $model;
    }

    public function with($relations)
    {
        return $this->model->with($relations);
    }

    public function getById($id, $with = null)
    {
        if(is_null($with)){
            return $this->model->findOrFail($id);
        }

        return $this->with($with)->findOrFail($id);
    }

    public function getAll($with = null)
    {
        if(is_null($with)){
            return $this->model->all();
        }

        return $this->with($with)->all();
    }

    public function create(array $data)
    {
        return $this->model->create($data);
    }

    public function update($id, array $data)
    {
        $update = $this->getById($id);
        return $update->update($data);
    }

    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    public function paginate($perPage = 5, $with = null)
    {
        if(is_null($with)){
            return $this->model->paginate($perPage);
        }

        return $this->with($with)->paginate($perPage);
    }
}