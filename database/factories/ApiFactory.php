<?php

use Faker\Generator as Faker;

$factory->define(App\Api::class, function (Faker $faker) {
    return [
        'user_id' => 1,
        'key' => RandKey(30),
        'expire_date' => \Carbon\Carbon::now()->addMonth(1)
    ];
});
