<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\PostRepository;

class BlogController extends Controller
{
    protected $postRepository;

    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function index()
    {
        return view('blog.index',[
            'posts' => $this->postRepository->paginate(5, ['User','Catalog'])
        ]);
    }

    public function post($id)
    {
        return view('blog.post',[
            'post' => $this->postRepository->getById($id, ['Catalog', 'Comment', 'User'])
        ]);
    }
}
