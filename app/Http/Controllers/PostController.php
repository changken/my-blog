<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Repositories\PostRepository;
use App\Repositories\UserRepository;
use App\Repositories\CatalogRepository;

class PostController extends Controller
{
    protected $postRepository;
    protected $userRepository;
    protected $catalogRepository;

    public function __construct(PostRepository $postRepository, UserRepository $userRepository, CatalogRepository $catalogRepository)
    {
        $this->postRepository = $postRepository;
        $this->userRepository = $userRepository;
        $this->catalogRepository = $catalogRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('post.index',[
            'posts' => $this->postRepository->paginate(5, ['Catalog', 'User'])
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('post.create', [
            'users' => $this->userRepository->getAll(),
            'catalogs' => $this->catalogRepository->getAll()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //處理文章資料
        $this->postRepository->create([
            'user_id' => $request->input('user_id'),
            'title' => $request->input('title'),
            'content' => $request->input('content')
        ],
            $request->input('catalog')
        );

        return view('tpl.msg',[
            'title' => '發布成功！',
            'status' => 1,
            'msg' => '發布成功！'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('post.show',[
            'post' => $this->postRepository->getById($id, ['Catalog', 'User'])
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = $this->postRepository->getById($id, 'Catalog');

        $catalogList = [];
        foreach ($post->Catalog as $catalog) {
            array_push($catalogList, $catalog->id);
        }

        return view('post.edit',[
            'users' => $this->userRepository->getAll(),
            'post' => $post,
            'catalogs' => $this->catalogRepository->getAll(),
            'catalogList' => $catalogList
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->postRepository->update(
            $id, [
                'user_id' => $request->input('user_id'),
                'title' => $request->input('title'),
                'content' => $request->input('content')
            ],
            $request->input('catalog')
        );

        return view('tpl.msg',[
            'title' => '修改成功！',
            'status' => 1,
            'msg' => '修改成功！'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->postRepository->delete($id);

        return view('tpl.msg',[
            'title' => '刪除成功！',
            'status' => 1,
            'msg' => '刪除成功！'
        ]);
    }
}
