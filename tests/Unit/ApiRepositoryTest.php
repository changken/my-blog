<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Repositories\ApiRepository;
use App\Api;
use App\User;
use App\Post;

class ApiRepositoryTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testGetPostsViaUser()
    {
        //Arrange
        $apiResposity = $this->app->make(ApiRepository::class);
        factory(User::class, 1)->create();
        factory(Post::class, 100)->create();
        factory(Api::class, 1)->create();

        //Act
        $result = $apiResposity->getPostsViaUser(1);

        //Assert
        $this->assertCount(100, $result);
    }
}
