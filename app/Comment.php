<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    /**
     * @var  array $guarded
     */
    protected $guarded = [
        'id',
        'created_at',
        'updated_at'
    ];

    public function Post()
	{
		return $this->belongsTo(Post::class);
	}
}
