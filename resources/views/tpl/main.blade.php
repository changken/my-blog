<html lang="zh-TW">

<head>
    <title>@yield('title')</title>
    <meta charset="UTF-8">
    @include('tpl.head')
    @section('head')
    @show
</head>

<body>
    <div class="container">
        <h1 class="text-center">@yield('title')</h1>
        @include('tpl.nav')
        <hr>
        @yield('content')
        <hr>
        @include('tpl.footer')
    </div>
</body>

</html>