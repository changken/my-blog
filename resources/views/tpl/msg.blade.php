@extends('tpl.main')

@section('title', $title)

@section('content')
    @if($status == 1)
        <div class="alert alert-success" role="alert">
            <p class="text-center">{{ $msg }}</p>
        </div>
    @else
        <div class="alert alert-danger" role="alert">
            <p class="text-center">{{ $msg }}</p>
        </div>
    @endif
@endsection