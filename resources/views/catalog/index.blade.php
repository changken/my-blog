@extends('tpl.main')

@section('title', 'All Catalogs')

@section('head')
    <script>
        function deleteCheck()
        {
            return confirm('確認要刪除嗎？');
        }
    </script>
@endsection

@section('content')
    @foreach($catalogs as $catalog)
        <table class="table table-bordered">
            <tr>
                <td>Catalog Name:</td>
                <td><a href="{{  route('catalog.show', ['catalog' => $catalog->id]) }}">{{ $catalog->catalog_name }}</a></td>
            </tr>
            <tr>
                <td>Created at:</td>
                <td>{{ $catalog->created_at }}</td>
            </tr>
            <tr>
                <td>Last updated at:</td>
                <td>{{ $catalog->updated_at }}</td>
            </tr>
            <tr>
                <td>Action:</td>
                <td>
                    <form action="{{ route('catalog.destroy', [ 'catalog' => $catalog->id ]) }}" method="post">
                        @csrf
                        @method('delete')
                        <button type="button" onclick="document.location.href='{{ route('catalog.edit', [ 'catalog' => $catalog->id ]) }}';" class="btn btn-warning"><i class="fa fa-edit"></i> 編輯</button>
                        <button type="submit" class="btn btn-danger" onclick="return deleteCheck()"><i class="fa fa-trash"></i> 刪除</button>
                    </form>
                </td>
            </tr>
        </table>
    @endforeach
    {{ $catalogs->links() }}
@endsection