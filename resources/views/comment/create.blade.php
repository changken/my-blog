@extends('tpl.main')

@section('title', 'New Comment')

@section('content')
    <form action="{{ url('/admin/comment') }}" method="post">
        @csrf
        <div class="form-group">
            <label for="PostID">Choose Post:</label>
            <select name="post_id" id="PostID" class="form-control">
                @foreach($posts as $post)
                    <option value="{{ $post->id  }}">{{ $post->title }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="NickName">NickName:</label>
            <input type="text" name="nickname" id="NickName" class="form-control" placeholder="Nick Name">
        </div>
        <div class="form-group">
            <label for="Email">Email:</label>
            <input type="text" name="email" id="Email" class="form-control" placeholder="Email">
        </div>
        <div class="form-group">
            <label for="Website">Website:</label>
            <input type="text" name="website" id="Website" class="form-control" placeholder="Website">
        </div>
        <div class="form-group">
            <label for="Comment">Your Comment:</label>
            <textarea name="comment" id="Comment" class="form-control" placeholder="Your Comment" rows="3"></textarea>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
@endsection