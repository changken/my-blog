@extends('tpl.front-main')

@section('title','Blog')

@section('content')
    @foreach($posts as $post)
        <table class="table table-bordered">
            <tr>
                <td><h2><a href="{{ route('blog.post', [ 'id' => $post->id ]) }}">{{ $post->title }}</a></h2></td>
            </tr>
            <tr>
                <td>Catalog:
                    @foreach($post->Catalog as $catalog)
                        <span class="badge badge-secondary" style="font-size: 16px;">{{ $catalog->catalog_name }}</span>
                    @endforeach
                </td>
            </tr>
            <tr>
                <td>Published By {{ $post->User->name }} at {{ $post->created_at }}</td>
            </tr>
        </table>
    @endforeach
    {{ $posts->links() }}
@endsection