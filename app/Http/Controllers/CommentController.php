<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\CommentRepository;
use App\Repositories\PostRepository;

class CommentController extends Controller
{
    protected $commentRepository;
    protected $postRepository;

    public function __construct(CommentRepository $commentRepository, PostRepository $postRepository)
    {
        $this->commentRepository = $commentRepository;
        $this->postRepository = $postRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('comment.index',[
            'comments' => $this->commentRepository->paginate(5)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('comment.create',[
            'posts' => $this->postRepository->getAll()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->commentRepository->create([
                'post_id' => $request->input('post_id'),
                'nickname' => $request->input('nickname'),
                'email' => $request->input('email'),
                'website' => $request->input('website'),
                'comment' => $request->input('comment'),
        ]);

        return view('tpl.msg', [
            'title' => '留言成功',
            'status' => 1,
            'msg' => '留言成功！'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('comment.show',[
            'comment' => $this->commentRepository->getById($id, 'Post')
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('comment.edit', [
            'posts' => $this->postRepository->getAll(),
            'comment' => $this->commentRepository->getById($id, 'Post')
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->commentRepository->update($id, [
            'post_id' => $request->input('post_id'),
            'nickname' => $request->input('nickname'),
            'email' => $request->input('email'),
            'website' => $request->input('website'),
            'comment' => $request->input('comment'),
        ]);

        return view('tpl.msg', [
            'title' => '修改成功!',
            'status' => 1,
            'msg' => '修改成功!'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->commentRepository->delete($id);

        return view('tpl.msg', [
            'title' => '刪除成功!',
            'status' => 1,
            'msg' => '刪除成功!'
        ]);
    }
}
