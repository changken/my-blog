@extends('tpl.main')

@section('title', $catalog->catalog_name . ' edit')

@section('content')
    <form action="{{ route('catalog.update', ['catalog' => $catalog->id]) }}" method="post">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="id">Catalog ID:</label>
            <input type="text" name="id" id="id" class="form-control" value="{{ $catalog->id }}" readonly="readonly">
        </div>
        <div class="form-group">
            <label for="catalog_name">Catalog Name:</label>
            <input type="text" name="catalog_name" id="catalog_name" class="form-control" value="{{ $catalog->catalog_name }}">
        </div>
        <div class="form-group">
            <label for="id">Created at:</label>
            <input type="text" name="created_at" id="created_at" class="form-control" value="{{ $catalog->created_at }}" readonly="readonly">
        </div>
        <div class="form-group">
            <label for="id">Updated at:</label>
            <input type="text" name="updated_at" id="updated_at" class="form-control" value="{{ $catalog->updated_at }}" readonly="readonly">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Update!</button>
        </div>
    </form>
@endsection