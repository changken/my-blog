@extends('tpl.main')

@section('title', 'New Post')

@section('content')
    <form action="{{ route('post.store') }}" method="post">
        @csrf
        <div class="form-group">
            <label for="user_id">User id:</label>
            <select class="form-control" name="user_id" id="user_id">
                @foreach($users as $user)
                    @if($user->id == Auth::id()){{--預設選擇為當前使用者--}}
                        <option value="{{ $user->id }}" selected>{{ $user->name }}</option>
                    @else
                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="title">Title:</label>
            <input type="text" name="title" id="title" class="form-control" placeholder="Post Title">
        </div>
        <div class="form-group">
            <label for="content">Content:</label>
            <textarea name="content" id="content" class="form-control" rows="3" placeholder="Post Content"></textarea>
        </div>
        <div class="form-group">
            <label for="catalog">Catalog:</label>
            <select name="catalog[]" id="catalog" class="form-control" multiple>
                @foreach($catalogs as $catalog)
                    <option value="{{ $catalog->id }}">{{ $catalog->catalog_name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
@endsection