@extends('tpl.main')

@section('title', 'Edit a comment')

@section('content')
    <form action="{{ route('comment.update', [ 'comment' => $comment->id ]) }}" method="post">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="PostID">Choose Post:</label>
            <select name="post_id" id="PostID" class="form-control">
                @foreach($posts as $post)
                    @if($post->id == $comment->post_id)
                        <option value="{{ $post->id  }}" selected>{{ $post->title }}</option>
                    @else
                        <option value="{{ $post->id  }}">{{ $post->title }}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="NickName">NickName:</label>
            <input type="text" name="nickname" id="NickName" class="form-control" placeholder="Nick Name" value="{{ $comment->nickname }}">
        </div>
        <div class="form-group">
            <label for="Email">Email:</label>
            <input type="text" name="email" id="Email" class="form-control" placeholder="Email" value="{{ $comment->email }}">
        </div>
        <div class="form-group">
            <label for="Website">Website:</label>
            <input type="text" name="website" id="Website" class="form-control" placeholder="Website" value="{{ $comment->website }}">
        </div>
        <div class="form-group">
            <label for="Comment">Your Comment:</label>
            <textarea name="comment" id="Comment" class="form-control" placeholder="Your Comment" rows="3">{{ $comment->comment }}</textarea>
        </div>
        <div class="form-group">
            <label for="Created_at">Created at:</label>
            <input type="text" name="created_at" id="Created_at" class="form-control" placeholder="Website" value="{{ $comment->created_at }}" readonly>
        </div>
        <div class="form-group">
            <label for="Updated_at">Updated at:</label>
            <input type="text" name="updated_at" id="Updated_at" class="form-control" placeholder="Website" value="{{ $comment->updated_at }}" readonly>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Edit</button>
        </div>
    </form>
@endsection