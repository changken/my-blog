<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Catalog extends Model
{
    /**
        * @var  array $guarded
        */
    protected $guarded = [
        'id',
        'created_at',
        'updated_at'
    ];

    public function Post()
    {
        return $this->belongsToMany(Post::class);
    }
}
