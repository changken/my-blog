@extends('tpl.front-main')

@section('title', $post->title)

@section('content')
    <table class="table table-bordered">
        <tr>
            <td><h1>{{ $post->title }}</h1></td>
        </tr>
        <tr>
            <td>{{ $post->content }}</td>
        </tr>
        <tr>
            <td>Catalog:
                @foreach($post->Catalog as $catalog)
                    <span class="badge badge-secondary" style="font-size:16px;">{{ $catalog->catalog_name }}</span>
                @endforeach
            </td>
        </tr>
        <tr>
            <td>Published By {{ $post->User->name }} at {{ $post->created_at }}</td>
        </tr>
        <tr>
            <td>Last updated at {{ $post->updated_at }}</td>
        </tr>
    </table>
    <h2>Comment:</h2>
    @foreach($post->Comment as $comment)
        <blockquote>
            <h2><a href="{{ $comment->website }}" target="_blank">{{ $comment->nickname }}</a></h2>
            <p>{{ $comment->comment  }}<br/>
            Published at {{ $comment->created_at }}</p>
        </blockquote>
    @endforeach
@endsection