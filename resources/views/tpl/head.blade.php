<link rel="stylesheet" href="{{ asset('/bootstrap/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset("/fontawesome/css/all.css") }}">
<link rel="stylesheet" href="{{ asset("/css/style.css") }}">
<script src="{{ asset('/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('/js/popper.min.js') }}"></script>
<script src="{{ asset('/bootstrap/js/bootstrap.min.js') }}"></script>