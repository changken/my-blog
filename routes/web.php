<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('/blog')->group(function() {
    Route::get('/', 'BlogController@index')->name('blog.home');
    Route::get('/{id}','BlogController@post')->name('blog.post');
});

Route::prefix('/admin')->middleware('isLogin')->group(function () {
    Route::resource('/post','PostController');
    Route::resource('/catalog','CatalogController');
    Route::resource('/comment','CommentController');
});

Route::prefix('/member')->middleware("isLogin")->group(function(){
    Route::prefix('/api')->middleware('can:super-admin')->group(function(){
        Route::get('/show','ApiController@index')->name('api.show');
        Route::get('/showAdd','ApiController@showAdd')->name('api.showAdd');
        Route::post('/add','ApiController@add')->name('api.add');
    });
});

Route::prefix("/api")->group(function (){
    Route::post("/getPosts", "ApiController@getPosts")->name("api.getPosts");
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
