@extends('tpl.main')

@section('title', 'All Comments')

@section('head')
    <script>
        function deleteCheck()
        {
            return confirm('確認要刪除嗎？');
        }
    </script>
@endsection

@section('content')
    @foreach($comments as $comment)
        <table class="table table-bordered">
            <tr>
                <td>NickName:</td>
                <td>{{ $comment->nickname }}</td>
            </tr>
            <tr>
                <td>Email:</td>
                <td>{{ $comment->email }}</td>
            </tr>
            <tr>
                <td>Website:</td>
                <td>{{ $comment->website }}</td>
            </tr>
            <tr>
                <td>Comment:</td>
                <td>{{ $comment->comment }}</td>
            </tr>
            <tr>
                <td>Created at:</td>
                <td>{{ $comment->created_at }}</td>
            </tr>
            <tr>
                <td>Last updated at:</td>
                <td>{{ $comment->updated_at }}</td>
            </tr>
            <tr>
                <td>Detail:</td>
                <td><a href="{{ route('comment.show', [ 'comment' => $comment->id ]) }}" class="btn btn-info">Detail</a></td>
            </tr>
            <tr>
                <td>Action:</td>
                <td>
                    <form action="{{ route('comment.destroy', [ 'comment' => $comment->id ]) }}" method="post">
                        @csrf
                        @method('delete')
                        <button type="button" onclick="document.location.href='{{ route('comment.edit', [ 'comment' => $comment->id ]) }}';" class="btn btn-warning"><i class="fa fa-edit"></i> 編輯</button>
                        <button type="submit" class="btn btn-danger" onclick="return deleteCheck()"><i class="fa fa-trash"></i> 刪除</button>
                    </form>
                </td>
            </tr>
        </table>
    @endforeach
    {{ $comments->links() }}
@endsection