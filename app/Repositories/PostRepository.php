<?php

namespace App\Repositories;

use App\Post;

class PostRepository extends Repository
{
    public function __construct(Post $post)
    {
        parent::__construct($post);
    }

    //Override
    public function create(array $data, array $relationships = null)
    {
        $create = parent::create($data);
        $create->Catalog()->attach($relationships);
        return $create;
    }

    //Override
    public function update($id, array $data, array $relationships = null)
    {
        $update = $this->getById($id);
        $update->Catalog()->sync($relationships);
        return $update->update($data);
    }

    //Override
    public function delete($id)
    {
        $delete = $this->getById($id);
        $delete->Catalog()->detach();//reset relationship with the catalog which has associated with this post
        return $delete->delete();
    }
}