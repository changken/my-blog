<ul class="nav nav-tabs">
    <li class="nav-item"><a href="{{ route('blog.home') }}" class="nav-link">Blog</a></li>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
           aria-expanded="false">
            Post <span class="caret"></span>
        </a>
        <div class="dropdown-menu">
            <a class="dropdown-item" href="{{ route('post.index') }}">Post List</a>
            <a class="dropdown-item" href="{{ route('post.create') }}">Create Post</a>
        </div>
    </li>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
           aria-expanded="false">
            Catalog <span class="caret"></span>
        </a>
        <div class="dropdown-menu">
            <a class="dropdown-item" href="{{ route('catalog.index') }}">Catalog List</a>
            <a class="dropdown-item" href="{{ route('catalog.create') }}">Create Catalog</a>
        </div>
    </li>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
           aria-expanded="false">
            Comment <span class="caret"></span>
        </a>
        <div class="dropdown-menu">
            <a class="dropdown-item" href="{{ route('comment.index') }}">Comment List</a>
            <a class="dropdown-item" href="{{ route('comment.create') }}">Create Comment</a>
        </div>
    </li>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
           aria-expanded="false">
            Api <span class="caret"></span>
        </a>
        <div class="dropdown-menu">
            <a class="dropdown-item" href="{{ route('api.show') }}">Api List</a>
            <a class="dropdown-item" href="{{ route('api.showAdd') }}">Add Api Key</a>
        </div>
    </li>
</ul>