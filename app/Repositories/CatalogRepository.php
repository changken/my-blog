<?php

namespace App\Repositories;

use App\Catalog;

class CatalogRepository extends Repository
{
    public function __construct(Catalog $catalog)
    {
        parent::__construct($catalog);
    }

    //Override
    public function delete($id)
    {
        $delete = $this->getById($id);
        $delete->Post()->detach();//reset relationship with the post which has associated with this catalog
        return $delete->delete();
    }
}