@extends('tpl.main')

@section('title', $post->title)

@section('content')
    <table class="table table-bordered">
        <tr>
            <td>Posts ID:</td>
            <td>{{ $post->id }}</td>
        </tr>
        <tr>
            <td>Title:</td>
            <td>{{ $post->title }}</td>
        </tr>
        <tr>
            <td>Content:</td>
            <td>{{ $post->content }}</td>
        </tr>
        <tr>
            <td>Catalog:</td>
            <td>
                @foreach($post->Catalog as $cata)
                    <span class="badge badge-secondary" style="font-size:15px;">{{ $cata->catalog_name }}</span>
                @endforeach
            </td>
        </tr>
        <tr>
            <td>Posts Author:</td>
            <td>{{ $post->User->name }}</td>
        </tr>
        <tr>
            <td>Created at:</td>
            <td>{{ $post->created_at }}</td>
        </tr>
        <tr>
            <td>Last updated at:</td>
            <td>{{ $post->updated_at }}</td>
        </tr>
    </table>
@endsection