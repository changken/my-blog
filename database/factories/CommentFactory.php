<?php

use Faker\Generator as Faker;

$factory->define(App\Comment::class, function (Faker $faker) {
    return [
        'post_id' => 1,
        'nickname' => $faker->name,
        'email' => $faker->email,
        'website' => $faker->url,
        'comment' => $faker->realText()
    ];
});
