<?php

namespace App\Repositories;

use App\Api;
use Carbon\Carbon;

class ApiRepository extends Repository
{
    public function __construct(Api $api)
    {
        parent::__construct($api);
    }

    /**
        * check api key if it is expired.
        * @param int $id
        * @return bool
        */
    public function isExpired($id)
    {
        return Carbon::now()->gt(
            Carbon::parse($this->getById($id)->expire_date)
        );
    }

    public function getPostsViaUser($id)
    {
        return $this->getById($id)->User->Post;
    }
}